# LeMo: Legal Monitor app

O LeMo é um app para monitoramento de processos legais. Ele expõe uma API REST para criação e deleção de processos. Essa api foi desenvolvida e documentada usando o [DRF](http://www.django-rest-framework.org). 

Os processos podem ser modificados através do admin do django.

O LeMo também simula um POST para um aplicativo de cliente informando modificações ocorridas nos processos. Esse FakePost foi implementado como um modelo e, a fim de garantir que ele sempre ocorrerá, a criação dos FakePosts foi vínculada à criação/modificação/deleção de Processos. Uma view específica foi implementada para visualização dos FakePosts.

Finalmente, foram feitos três testes unitários para checar que a criação de FakePosts está de fato vinculada às modificações nos Processos.

A aplicação foi desenvolvida e testada em python3.6 e está disponível no endereço https://lemoapp.herokuapp.com/


## Dependências
- Django==1.11.4
- djangorestframework==3.6.3

## Instruções de instalação

- clone este repo
    - `git clone <REPO>; cd tikal`
- (opcional) crie um virtualenv 
    - `virtualenv venv; source venv/bin/activate`
- instale as dependências 
    - `pip install django djangorestframework`
- rode os testes
    - `./manage.py test`
- crie o banco
    - `./manage.py migrate`
    
- finalmente, rode o servidor do django
    - `./manage.py runserver`
    
O serviço estará disponível em localhost:8000.

## Instruções de uso

### Documentação

Neste documento darei exemplos de como fazer usar a API com `curl`, mas a [documentação automática](https://lemoapp.herokuapp.com/) do DRF tem exemplos em python, javascript e com o cli coreapi. 

### API REST
Ambos os endpoints REST exigem autenticação. Para acessar a versão *deployada* no heroku, use as o login `admin` e senha `voltaocaoarrependido` 

#### Criar Processo (POST /api/processo/)

A entrada é um JSON com o `numero_processo` e `dados_processo`. 

```bash
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{"numero_processo": "00000000000000000009", "dados_processo": "um novo processo"}' 'https://admin:voltaocaoarrependido@lemoapp.herokuapp.com/api/processo/'

```

#### Deletar Processo (DELETE /api/processo/\<numero_processo\>/)

```bash
# deletar o processso 12345678900987654321
curl -X "DELETE" 'https://admin:voltaocaoarrependido@lemoapp.herokuapp.com/api/processo/12345678900987654321/'

```

#### Interface admin (/admin/)

Link no heroku: [/admin/](https://lemoapp.herokuapp.com/admin/). 

Use as mesmas credenciais mencionadas acima. Somente na interface admin é possível modificar o conteúdo dos processos.


#### FakePosts (/fakeposts)

Link no heroku: [/fakeposts](https://lemoapp.herokuapp.com/fakeposts/)

Todas as alterações são listadas.