from django.db import models
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.core.validators import RegexValidator

__all__ = ['Processo', 'FakePost']


class Processo(models.Model):
    numero_processo = models.CharField(
        verbose_name='Número de processo',
        max_length=20, unique=True,
        validators=[
            RegexValidator(
                regex=r'\d{20}',
                message='Insira um número com 20 caracteres.'),
        ])
    dados_processo = models.TextField(verbose_name='Dados do processo')

    def __str__(self):
        return self.numero_processo


class FakePost(models.Model):
    processo = models.ForeignKey(
        "Processo",
        null=True,
        on_delete=models.SET_NULL,
        # postgres requires db_constraint set to False
        # https://code.djangoproject.com/ticket/27315
        db_constraint=False,
    )
    conteudo = models.TextField(verbose_name='Conteúdo')
    modificado_em = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-modificado_em']


@receiver(post_save, sender=Processo)
def criar_atualizar_processo(sender, instance=None, created=False, **kwargs):

    conteudo = dict(num=instance.numero_processo,
                    dados=instance.dados_processo)

    if created:
        msg = 'O processo {num} foi <strong>criado</strong> com os seguintes dados:\n\n{dados}'
    else:
        msg = 'O processso {num} foi <strong>alterado</strong> para:\n\n{dados}'

    FakePost.objects.create(processo=instance, conteudo=msg.format(**conteudo))


@receiver(pre_delete, sender=Processo)
def deletar_processo(sender, instance=None, **kwargs):

    msg = 'O processso {} foi <strong>deletado</strong>.'.format(instance.numero_processo)

    FakePost.objects.create(processo=instance, conteudo=msg)
