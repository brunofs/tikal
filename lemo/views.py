
from django.views.generic import ListView
from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from .models import Processo, FakePost
from .serializers import ProcessoSerializer


class CreateDestroyViewSet(mixins.CreateModelMixin,
                           mixins.DestroyModelMixin,
                           viewsets.GenericViewSet):
    """
    This viewset that provides actions to create and destroy models.
    """
    pass


class ProcessoViewSet(CreateDestroyViewSet):
    """
    create:
    Create a new Processo

    delete:
    Delete a Processo
    """
    queryset = Processo.objects.all()
    serializer_class = ProcessoSerializer
    lookup_field = 'numero_processo'
    permission_classes = [IsAuthenticatedOrReadOnly]


class FakePostView(ListView):
    """
    List FakePosts
    """
    model = FakePost