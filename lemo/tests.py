from django.test import TestCase
from .models import *


class TestFakePost(TestCase):
    def setUp(self):
        super(TestFakePost, self).setUp()
        self.processo = Processo.objects.create(
            numero_processo='{:020d}'.format(1), dados_processo='criado')

    def test_check_created_fakepost(self):
        # obj created on setup must have created a FakePost
        self.assertEqual(FakePost.objects.count(), 1)

    def test_modify_processo(self):
        # modify processo
        self.processo.dados_processo = 'alterado'
        self.processo.save()

        # after changes, another FakePost must be created
        self.assertEqual(FakePost.objects.count(), 2)

    def test_delete_processo(self):
        # delete processo
        self.processo.delete()

        # after deletion, another FakePost must be created
        self.assertEqual(FakePost.objects.count(), 2)